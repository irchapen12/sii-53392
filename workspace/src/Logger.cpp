#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

int main (void){
	char nombre[10];
	int fd, puntos;
	int check_err;
	
	if(mkfifo("/tmp/PipeLogger",0666)<0)
	{
		perror("mkfifo logger");
		return 1;
	}
	
	fd = open("/tmp/PipeLogger",O_RDONLY);
	 
	if (fd==-1){
		perror("open");
		return 1;
		}
	while(check_err=read(fd,&nombre,10)>0){
	if(check_err<0){
		perror("read_nombre");
		return 1;
	}
			
	if(read (fd,&puntos,sizeof(int))<0){
		perror("read_puntos");
		return 1;
	}
	printf("punto para %s, lleva un total de %d puntos.\n",nombre, puntos);
	if (puntos==3)
		printf("¡Victoria para %s!\n",nombre);
	
	}
	if(close(fd)<0){
		perror("close");
		return 1;
	}
	if(unlink("/tmp/PipeLogger")<0){
		perror("unlink");
		return 1;
	}
	return 0; 
}
